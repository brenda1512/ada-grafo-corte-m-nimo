#include "_claGraph.h"


using namespace std;

void funReadFile(_claGraph<int,int> &myGraph, char * parArchivo)
{
    ifstream varFileStream;
    varFileStream.open(parArchivo);
    char varBufferC[256];
    string varBuffer;
    int varStartNode;
    int varEndNode;

    while(varFileStream.getline(varBufferC,256))
    {
        varBuffer = varBufferC;
        varStartNode = atoi(varBuffer.substr(0, varBuffer.find('\t')).c_str());
        varBuffer = varBuffer.substr(0,varBuffer.find_last_of('\t'));

        while(varBuffer.find_last_of('\t') != -1)
        {
            varEndNode = atoi(varBuffer.substr(varBuffer.find_last_of('\t')+1).c_str());
            varBuffer = varBuffer.substr(0,varBuffer.find_last_of('\t'));
            myGraph.metInsertEdge(varStartNode,varEndNode);
        }
    }

}

int rdtsc()
{
    __asm__ __volatile__("rdtsc");
}

void funDeleteLoops(_claGraph<int,int> & myGraph)
{
    auto vv = myGraph.varEdges;
    for(auto & var : vv)
    {
        if(var->varNodes[0]->varData == var->varNodes[1]->varData)
        {
            myGraph.metDeleteEdge(var);
            myGraph.varEdges.erase(find(myGraph.varEdges.begin(),myGraph.varEdges.end(), var));
        }
    }
}

void funMergeNodes(_claGraph<int, int> & myGraph)
{
    int varEdgesSize = myGraph.varEdges.size();
    if(!varEdgesSize)
    {
        return;
    }

    int varRandom = rand() % varEdgesSize;

    auto varCurrentEdge = myGraph.varEdges[varRandom];

    myGraph.metDeleteEdge(myGraph.varEdges[varRandom]);
    myGraph.varEdges.erase(find(myGraph.varEdges.begin(),myGraph.varEdges.end(), varCurrentEdge));

    auto varNodeEndEdges = varCurrentEdge->varNodes[1]->varEdges;
    for(auto & varNodeEndEdge : varNodeEndEdges)
    {
        if(varNodeEndEdge->varNodes[0]->varData == varCurrentEdge->varNodes[1]->varData)
        {
            myGraph.metModifyEdge(varCurrentEdge->varNodes[0]->varData, varNodeEndEdge->varNodes[1]->varData);
            myGraph.varEdges.erase(find(myGraph.varEdges.begin(),myGraph.varEdges.end(), varNodeEndEdge));
            myGraph.metDeleteEdge(varNodeEndEdge);
        }
        else
        {
            myGraph.metModifyEdge(varCurrentEdge->varNodes[0]->varData, varNodeEndEdge->varNodes[0]->varData);
            myGraph.varEdges.erase(find(myGraph.varEdges.begin(),myGraph.varEdges.end(), varNodeEndEdge));
            myGraph.metDeleteEdge(varNodeEndEdge);

        }
    }
    myGraph.metDeleteNode(varCurrentEdge->varNodes[1]->varData);
}

int minCut(_claGraph<int,int> &myGraph)
{
    srand(rdtsc());

    while(myGraph.varNodes.size()>2)
    {
        funMergeNodes(myGraph);
        funDeleteLoops(myGraph);
    }

    return myGraph.varEdges.size();
}

int main()
{
    int A = 200;
    int m = 200;
    while(A--)
    {
        _claGraph <int,int> myGraph;
        funReadFile(myGraph, "mincut.txt");
        int f = minCut(myGraph);
        if(f<m)
            m=f;
    }
    cout << m;
    return 0;
}
