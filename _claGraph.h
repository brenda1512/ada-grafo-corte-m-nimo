#ifndef _CLAGRAPH_H
#define _CLAGRAPH_H

#include "includes.h"
#include "_claEdge.h"
#include "_claNode.h"

using namespace std;

template <class _N, class _E> // _N : Node datatype, _E : Edge datatype
class _claGraph
{
    public:
        typedef _claGraph<_N,_E> self;
        typedef _claNode<self> claNode;
        typedef _claEdge<self> claEdge;
        typedef _N N;
        typedef _E E;

        vector<claNode*> varNodes;
        vector<claEdge*> varEdges;
        _claGraph (){}
        bool metInsertNode(N);
        bool metInsertEdge(N,N);
        bool metDeleteNode(N);
        bool metDeleteEdge(N,N);
        bool metDeleteEdge(claEdge *);
        bool metModifyEdge(N,N);
        int metFindNode(N);
        void metPrint();
};

template <class _N, class _E>
int _claGraph<_N,_E>::metFindNode(N parData)
{
    int i=0;
    for(auto & varElem : varNodes)
    {
        if(varElem->varData == parData)
            return i;
        i++;
    }
    return -1;
}

template <class _N,class _E>
bool _claGraph<_N,_E>::metInsertNode(_N parData)
{
    if(metFindNode(parData)!=-1)
        return false;

    auto varNode = new claNode();
    varNode->varData = parData;
    varNodes.push_back(varNode);

    return 1;
}

template <class _N,class _E>
bool _claGraph<_N,_E>::metInsertEdge(_N parDataStart, _N parDataEnd)
{
    int varPosStart = metFindNode(parDataStart);
    int varPosEnd = metFindNode(parDataEnd);
    if(varPosStart == -1)
        metInsertNode(parDataStart);
    if(varPosEnd == -1)
        metInsertNode(parDataEnd);

    varPosStart = metFindNode(parDataStart);
    varPosEnd = metFindNode(parDataEnd);

    for(auto & varElem : varNodes.at(varPosStart)->varEdges)
    {
        if(varElem->varNodes[0]->varData == parDataEnd or varElem->varNodes[1]->varData == parDataEnd)
            return true;
    }

    auto varEdge = new claEdge();

    varEdge->varNodes[0] = varNodes.at(varPosStart);
    varEdge->varNodes[1] = varNodes.at(varPosEnd);

    varNodes.at(varPosStart)->varEdges.push_back(varEdge);
    varNodes.at(varPosEnd)->varEdges.push_back(varEdge);
    varEdges.push_back(varEdge);
    return true;
}



template <class _N,class _E>
bool _claGraph<_N,_E>::metModifyEdge(_N parDataStart, _N parDataEnd)
{
    int varPosStart = metFindNode(parDataStart);
    int varPosEnd = metFindNode(parDataEnd);

    auto varEdge = new claEdge();

    varEdge->varNodes[0] = varNodes.at(varPosStart);
    varEdge->varNodes[1] = varNodes.at(varPosEnd);

    varNodes.at(varPosStart)->varEdges.push_back(varEdge);
    varNodes.at(varPosEnd)->varEdges.push_back(varEdge);
    varEdges.push_back(varEdge);
    return true;
}



template <class _N,class _E>
bool _claGraph<_N,_E>::metDeleteNode(_N parData)
{
    int varPosNode = metFindNode(parData);
    if(varPosNode == -1)
        return false;

    for(auto & varElem : varNodes.at(varPosNode)->varEdges)
    {
        metDeleteEdge(varElem);
    }

    varNodes.erase(varNodes.begin()+varPosNode);
    return true;
}


template <class _N,class _E>
bool _claGraph<_N,_E>::metDeleteEdge(claEdge * parEdge)
{
    for(auto & varEdge : parEdge->varNodes[0]->varEdges)
    {
        if(varEdge == parEdge)
        {
            parEdge->varNodes[0]->varEdges.erase(find(varEdge->varNodes[0]->varEdges.begin(), varEdge->varNodes[0]->varEdges.end(), varEdge));
            break;
        }
    }

    for(auto & varEdge : parEdge->varNodes[1]->varEdges)
    {
        if(varEdge == parEdge)
        {
            parEdge->varNodes[1]->varEdges.erase(find(varEdge->varNodes[1]->varEdges.begin(), varEdge->varNodes[1]->varEdges.end(), varEdge));
            break;
        }
    }
    return true;
}

template <class _N,class _E>
void _claGraph<_N,_E>::metPrint()
{
    for(auto & varElem : varNodes)
    {
        cout << varElem->varData << " :\t";
        for(auto & varElem2 : varElem->varEdges)
        {
            cout << "(" << varElem2->varNodes[0]->varData << ", " << varElem2->varNodes[1]->varData << ")\t";// << varElem2 ->varData << ")" << "\t";
        }
        cout << endl;
    }
}


#endif // _CLAGRAPH_H

